package shop.velox.price.dao;

import static java.time.LocalDateTime.now;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.util.List;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import org.springframework.data.jpa.domain.Specification;
import shop.velox.price.api.dto.PriceStatus;
import shop.velox.price.model.PriceEntity;

public class PriceSpecification {

  public static Specification<PriceEntity> isArticleIdIn(List<String> articleIds) {
    return (root, query, builder) -> {
      if (isNotEmpty(articleIds)) {
        return root.get("articleId").in(articleIds);
      } else {
        return null;
      }
    };
  }

  public static Specification<PriceEntity> isCurrencyEqual(String currencyId) {
    return (root, query, builder) -> {
      if (isNotBlank(currencyId)) {
        Join<Object, Object> priceToCurrencyJoin = root.join("currency");
        Path<Object> idPath = priceToCurrencyJoin.get("id");
        return builder.equal(idPath, currencyId);
      } else {
        return null;
      }
    };
  }

  public static Specification<PriceEntity> hasStatus(PriceStatus priceStatus) {
    return (root, query, builder) -> {
      if (PriceStatus.ACTIVE.equals(priceStatus)) {
        return builder.and(
            builder.isTrue(root.get("active")),
            builder.lessThan(root.get("validFrom"), now()),
            builder.greaterThan(root.get("validTo"), now())
        );
      } else if (PriceStatus.INACTIVE.equals(priceStatus)) {
        return builder.or(
            builder.isFalse(root.get("active")),
            builder.lessThan(root.get("validTo"), now())
        );
      } else if (PriceStatus.UPCOMING.equals(priceStatus)) {
        return builder.and(
            builder.isTrue(root.get("active")),
            builder.greaterThan(root.get("validFrom"), now())
        );
      } else {
        return null;
      }
    };
  }

}
