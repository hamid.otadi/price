package shop.velox.price.dao;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import shop.velox.price.model.CurrencyEntity;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long> {

  Optional<CurrencyEntity> findOneById(String id);

}
