package shop.velox.price.api.dto;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.lang.Nullable;

public class ArticleDto {

  private final String id;

  private final BigDecimal quantity;

  private final BigDecimal totalPrice;

  private final BigDecimal unitPrice;

  /**
   * @param id          id of the article
   * @param quantity    desired quantity of the article
   * @param totalPrice  total price of the article for desired quantity, null in the request for article price list
   * @param unitPrice   unit Price of the article, null in the request for article price list
   */
  public ArticleDto(String id, BigDecimal quantity, @Nullable BigDecimal totalPrice,
      @Nullable BigDecimal unitPrice) {
    this.id = id;
    this.quantity = quantity;
    this.totalPrice = totalPrice;
    this.unitPrice = unitPrice;
  }

  public ArticleDto(String id, BigDecimal quantity) {
    this(id, quantity, null, null);
  }

  public ArticleDto() {
    this(null, null, null, null);
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getTotalPrice() {
    return totalPrice;
  }

  public String getId() {
    return id;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

}
