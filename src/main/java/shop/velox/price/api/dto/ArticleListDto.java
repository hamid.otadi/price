package shop.velox.price.api.dto;

import java.util.List;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ArticleListDto {

  List<ArticleDto> articles;

  public ArticleListDto() {
    this(null);
  }

  public ArticleListDto(final List<ArticleDto> articles) {
    this.articles = ListUtils.emptyIfNull(articles);
  }

  public List<ArticleDto> getArticles() {
    return articles;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
