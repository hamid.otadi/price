package shop.velox.price.api.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PriceDto {

  private static final Logger LOG = LoggerFactory.getLogger(PriceDto.class);

  private String id;

  @Size(min = 3, max = 50)
  private String articleId;

  private LocalDateTime validFrom;

  @Future
  private LocalDateTime validTo;

  @PositiveOrZero
  private BigDecimal minQuantity;

  @NotNull
  @PositiveOrZero
  private BigDecimal unitPrice;

  private Boolean active;

  @NotNull
  private CurrencyDto currency;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getArticleId() {
    return articleId;
  }

  public void setArticleId(String articleId) {
    this.articleId = articleId;
  }

  public LocalDateTime getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(LocalDateTime validFrom) {
    this.validFrom = validFrom;
  }

  public LocalDateTime getValidTo() {
    return validTo;
  }

  public void setValidTo(LocalDateTime validTo) {
    this.validTo = validTo;
  }

  public BigDecimal getMinQuantity() {
    return minQuantity;
  }

  public void setMinQuantity(BigDecimal minQuantity) {
    this.minQuantity = minQuantity;
  }

  public BigDecimal getUnitPrice() {
    return unitPrice;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.unitPrice = unitPrice;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public CurrencyDto getCurrency() {
    return currency;
  }

  public void setCurrency(CurrencyDto currency) {
    this.currency = currency;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }
}
