package shop.velox.price.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class CurrencyDto {

  @Schema(description = "Id of the currency.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private String id;

  @Schema(description = "IsoCode of the currency)", required = true)
  private String isoCode;

  @Schema(description = "Name of the currency", required = true)
  private String name;

  @Schema(description = "Symbol of the Currency", required = true)
  private String symbol;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

}
