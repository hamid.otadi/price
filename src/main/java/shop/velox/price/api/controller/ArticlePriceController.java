package shop.velox.price.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.math.BigDecimal;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import shop.velox.price.api.dto.ArticleDto;
import shop.velox.price.api.dto.ArticleListDto;


@Tag(name = "ArticlePriceList", description = "the ArticlePriceList API")
@RequestMapping("")
public interface ArticlePriceController {

  @Operation(summary = "gets applied price for a list of articles at the specified quantity", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "returns the price for all the articles in the request", content = @Content(schema = @Schema(implementation = ArticleListDto.class))),
      @ApiResponse(responseCode = "206", description = "Not all the articles in the request have a price. Returning only the ones which have it.", content = @Content(schema = @Schema(implementation = ArticleListDto.class))),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Article Id)", content = @Content(schema = @Schema())),
  })
  @PostMapping(value = "/articles/prices", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<ArticleListDto> getPrices(@RequestBody ArticleListDto articleListDto);

  @Operation(summary = "gets applied price for an article at the specified quantity", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "returns the price for the article in the request", content = @Content(schema = @Schema(implementation = ArticleDto.class))),
      @ApiResponse(responseCode = "404", description = "Specified article does not have a price", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Article Id)", content = @Content(schema = @Schema())),
  })
  @GetMapping(value = "/article/{id}/prices/{quantity}", produces = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<ArticleDto> getPriceForQuantity(
      @Parameter(description = "Id of the Article. Cannot be empty.", required = true) @PathVariable("id") String id,
      @Parameter(description = "Desired Quantity. Cannot be empty.", required = true) @PathVariable("quantity") BigDecimal quantity);
}
