package shop.velox.price.api.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.controller.CurrencyController;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.service.CurrencyService;

@RestController
public class CurrencyControllerImpl implements CurrencyController {

  private static final Logger LOG = LoggerFactory.getLogger(CurrencyControllerImpl.class);

  private final CurrencyService currencyService;

  private final Converter<CurrencyEntity, CurrencyDto> currencyConverter;

  public CurrencyControllerImpl(@Autowired CurrencyService currencyService,
      @Autowired Converter<CurrencyEntity, CurrencyDto> currencyConverter) {
    this.currencyService = currencyService;
    this.currencyConverter = currencyConverter;
  }

  @Override
  public ResponseEntity<Page<CurrencyDto>> getCurrencies(Pageable pageable) {
    Page<CurrencyEntity> entitiesPage = currencyService.findAll(pageable);
    Page<CurrencyDto> dtosPage = currencyConverter.convert(pageable, entitiesPage);
    return ResponseEntity.ok(dtosPage);
  }

  @Override
  public ResponseEntity<CurrencyDto> createCurrency(String customerId, CurrencyDto currency) {
    LOG.info("{} is creating Currency: {}", customerId, currency);
    CurrencyDto body = currencyConverter.convertEntityToDto(currencyService.createCurrency(
        currencyConverter.convertDtoToEntity(currency)));
    LOG.info("{} has created Currency: {}", customerId, body);
    return ResponseEntity.status(HttpStatus.CREATED).body(body);
  }

  @Override
  public ResponseEntity<CurrencyDto> getCurrency(String id) {
    return currencyService.findOne(id)
        .map(currencyConverter::convertEntityToDto)
        .map(ResponseEntity::ok)
        .orElse(ResponseEntity.notFound().build());
  }

  @Override
  public ResponseEntity<CurrencyDto> updateCurrency(String id, CurrencyDto currencyDto) {
    LOG.info("updateCurrency {}, {}", id, currencyDto);
    CurrencyEntity currencyEntity = currencyService
        .update(id, currencyConverter.convertDtoToEntity(currencyDto));
    return ResponseEntity.ok(currencyConverter.convertEntityToDto(currencyEntity));
  }

  @Override
  public ResponseEntity<Void> removeCurrency(String id) {
    LOG.info("removeCurrency {}", id);
    currencyService.removeCurrency(id);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

}
