package shop.velox.price.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.api.dto.PriceStatus;

@Tag(name = "Price", description = "the Price API")
@RequestMapping("")
public interface PriceController {

  String PRICES_PATH = "/prices";
  String CURRENCY_ID_FILTER = "currencyId";
  String ARTICLE_ID_FILTER = "articleId";
  String PRICE_STATUS_FILTER = "statusFilter";

  @Operation(summary = "Get Prices, sorted by startDate ascending", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = PriceDto.class))),
  })
  @GetMapping(value = PRICES_PATH)
  ResponseEntity<Page<PriceDto>> getPrices(
      @SortDefault.SortDefaults({@SortDefault(sort = "validFrom", direction = Sort.Direction.ASC)})
          Pageable pageable,
      @Parameter(description = "Filter by Id of the Article. Return all availabilities matching of the given articleIds")
      @RequestParam(name = ARTICLE_ID_FILTER, required = false) final List<String> articleIds,
      @Parameter(description = "Return prices with different status (all, active, upcoming, inactive) of the Article. If statusFilter is empty, return all prices. Can be empty.")
      @RequestParam(name = PRICE_STATUS_FILTER, required = false) final PriceStatus filter,
      @Parameter(description = "Return prices for the given currency ID.")
      @RequestParam(name = CURRENCY_ID_FILTER, required = false) final String currencyId
  );

  @Operation(summary = "Get Price by id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "404", description = "price not found")
  })
  @GetMapping(value = PRICES_PATH + "/{id}")
  ResponseEntity<PriceDto> getPrice(
      @Parameter(description = "Id of the Price. Cannot be empty.", required = true) @PathVariable("id") final String id);

  @Operation(summary = "creates Price", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "returns the created price", content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "422", description = "Mandatory parameters are missing. (e.g. Article Id)", content = @Content(schema = @Schema())),
  })
  @PostMapping(value = PRICES_PATH, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<PriceDto> createPrice(
      @Parameter(hidden = true) @AuthenticationPrincipal(expression = "T(shop.velox.commons.security.utils.AuthUtils).principalToCustomerIdMapper.apply(#this)") String customerId,
      @Parameter(description = "Price to create. Cannot be empty.", required = true) @Valid @RequestBody PriceDto prices);

  @Operation(summary = "Updates a price. The updated price will have a new id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = PriceDto.class))),
      @ApiResponse(responseCode = "404", description = "price not found", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "409", description = "id/articledId cannot be changed", content = @Content(schema = @Schema())),
      @ApiResponse(responseCode = "422", description = "Mandatory data missing", content = @Content(schema = @Schema()))
  })
  @PatchMapping(value = PRICES_PATH + "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  ResponseEntity<PriceDto> updatePrice(
      @Parameter(description = "Id of the Price. Cannot be empty.", required = true) @PathVariable("id") String id,
      @Parameter(description = "Price to update. Cannot be empty.", required = true) @Valid @RequestBody PriceDto priceDto);

  @Operation(summary = "Delete Price by id", description = "")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "successful operation"),
      @ApiResponse(responseCode = "404", description = "price not found")
  })
  @DeleteMapping(value = PRICES_PATH + "/{id}")
  ResponseEntity<Void> removePrice(
      @Parameter(description = "Id of the Price. Cannot be empty.", required = true) @PathVariable("id") final String id);
}
