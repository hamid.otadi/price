package shop.velox.price.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotEmpty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import shop.velox.commons.model.AbstractEntity;

@Entity
@Table(uniqueConstraints = {
    @UniqueConstraint(name = CurrencyEntity.COMPOSITE_CONSTRAINT_NAME, columnNames = {"isoCode"}),
})
public class CurrencyEntity extends AbstractEntity {

  public static final String COMPOSITE_CONSTRAINT_NAME = "Currency_Unique_Constraint";

  @PrePersist
  private void prePersistFunction() {
    if (StringUtils.isEmpty(id)) {
      id = UUID.randomUUID().toString();
    }
  }

  @Column(unique = true, nullable = false)
  private String id;

  @Column(nullable = false)
  @NotEmpty
  private String isoCode;

  @Column(nullable = false)
  @NotEmpty
  private String name;

  @Column(nullable = false)
  @NotEmpty
  private String symbol;

  public CurrencyEntity() {
    // For JPA
  }

  public CurrencyEntity(String isoCode, String name, String symbol) {
    this.isoCode = isoCode;
    this.name = name;
    this.symbol = symbol;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSymbol() {
    return symbol;
  }

  public void setSymbol(String symbol) {
    this.symbol = symbol;
  }
}
