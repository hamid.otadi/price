package shop.velox.price.converter.impl;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.commons.converter.Converter;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.model.CurrencyEntity;

@Component
public class CurrencyConverter implements Converter<CurrencyEntity, CurrencyDto> {

  private static final Logger LOG = LoggerFactory.getLogger(CurrencyConverter.class);
  private final MapperFacade mapperFacade;

  public CurrencyConverter() {
    MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
    mapperFactory
        .classMap(CurrencyEntity.class, CurrencyDto.class)
        .byDefault()
        .register();

    mapperFacade = mapperFactory.getMapperFacade();
  }

  @Override
  public CurrencyDto convertEntityToDto(CurrencyEntity currencyEntity) {
    CurrencyDto currencyDto = getMapperFacade().map(currencyEntity, CurrencyDto.class);
    LOG.debug("Converted {} to {}", currencyEntity, currencyDto);
    return currencyDto;
  }

  @Override
  public CurrencyEntity convertDtoToEntity(CurrencyDto currencyDto) {
    CurrencyEntity currencyEntity = getMapperFacade().map(currencyDto, CurrencyEntity.class);
    LOG.debug("Converted {} to {}", currencyDto, currencyEntity);
    return currencyEntity;
  }

  public MapperFacade getMapperFacade() {
    return mapperFacade;
  }
}
