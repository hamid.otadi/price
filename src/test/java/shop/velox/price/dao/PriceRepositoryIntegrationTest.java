package shop.velox.price.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.annotation.Resource;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import shop.velox.price.PriceApplicationTests;
import shop.velox.price.model.CurrencyEntity;
import shop.velox.price.model.PriceEntity;

@ExtendWith(SpringExtension.class)
@Transactional
class PriceRepositoryIntegrationTest extends PriceApplicationTests {

  private static final Logger LOG = LoggerFactory.getLogger(PriceRepositoryIntegrationTest.class);

  @Resource
  private PriceRepository priceRepository;

  @Resource
  private CurrencyRepository currencyRepository;

  @Test
  @Order(1)
  public void auditTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();

    PriceEntity savedEntity = priceRepository
        .save(new PriceEntity("articleId", BigDecimal.valueOf(123.45), currencyEntity));

    assertNotNull(savedEntity.getCreateTime());
    assertNotNull(savedEntity.getModifiedTime());
  }

  @Test
  @Order(2)
  public void CompositeConstraintUniqueTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();

    String articleId = "articleId";
    double priceValue = 123.45;
    priceRepository
        .save(new PriceEntity(articleId, BigDecimal.valueOf(priceValue), currencyEntity));
    assertEquals(1L, priceRepository.findAll(Pageable.unpaged()).getTotalElements());

    Assertions.assertThrows(
        DataIntegrityViolationException.class,
        () -> priceRepository
            .save(new PriceEntity(articleId, BigDecimal.valueOf(priceValue), currencyEntity)));
  }

  @Test
  @Order(3)
  public void findAllByFiltersTest() {
    // If
    CurrencyEntity currencyEntity = getCurrencyEntity();

    var articleId = "articleId";
    priceRepository.save(
        new PriceEntity(articleId, BigDecimal.valueOf(0L), BigDecimal.valueOf(123.45),
            currencyEntity));

    priceRepository.save(
        new PriceEntity(articleId, BigDecimal.valueOf(3L), BigDecimal.valueOf(123.44),
            currencyEntity));

    priceRepository
        .save(new PriceEntity("otherArticleId", BigDecimal.valueOf(123.45), currencyEntity));

    PriceEntity inactive = new PriceEntity(articleId, BigDecimal.valueOf(2L),
        BigDecimal.valueOf(123.45), currencyEntity);
    inactive.setActive(false);
    priceRepository.save(inactive);

    PriceEntity notStarted = new PriceEntity(articleId, LocalDateTime.now().plusDays(10),
        LocalDateTime.now().plusDays(20), BigDecimal.valueOf(123.45), currencyEntity);
    priceRepository.save(notStarted);

    PriceEntity expired = new PriceEntity(articleId, LocalDateTime.now().minusDays(20),
        LocalDateTime.now().minusDays(10), BigDecimal.valueOf(123.45), currencyEntity);
    priceRepository.save(expired);

    PriceEntity minQuantityTooHigh = new PriceEntity(articleId, BigDecimal.TEN,
        BigDecimal.valueOf(123.45), currencyEntity);
    priceRepository.save(minQuantityTooHigh);

    // When
    Page<PriceEntity> results = priceRepository
        .findAllByArticleIdAndActiveIsTrueAndValidFromIsBeforeAndValidToIsAfterAndMinQuantityIsLessThanEqualOrderByUnitPriceAsc(
            Pageable.unpaged(),
            articleId, LocalDateTime.now(), LocalDateTime.now(), BigDecimal.valueOf(5));

    // Then
    assertEquals(2, results.getContent().size());
    assertEquals(articleId, results.getContent().get(0).getArticleId());
    assertEquals(articleId, results.getContent().get(1).getArticleId());
    assertTrue(
        results.getContent().get(0).getUnitPrice().doubleValue() <= results.getContent().get(1)
            .getUnitPrice().doubleValue());
  }

  @Test
  @Order(4)
  public void findByIdTest() {
    CurrencyEntity currencyEntity = getCurrencyEntity();

    String articleId = "articleId";
    double priceValue = 123.45;
    PriceEntity existing = priceRepository
        .save(new PriceEntity(articleId, BigDecimal.valueOf(priceValue), currencyEntity));
    assertEquals(1L, priceRepository.findAll(Pageable.unpaged()).getTotalElements());

    assertThat(priceRepository.findOneByIdAndActiveIsTrue(existing.getId())).isNotEmpty();

    existing.setActive(false);
    priceRepository.save(existing);

    assertThat(priceRepository.findOneByIdAndActiveIsTrue(existing.getId())).isEmpty();
  }

  private CurrencyEntity getCurrencyEntity() {
    return currencyRepository.save(new CurrencyEntity("CHF", "Swiss franc", "CHF"));
  }

}
